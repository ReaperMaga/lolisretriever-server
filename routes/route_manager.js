const validator = require("../validator")

const exportValues = {
    init(app) {
        app.use('/', require('./pages/index'));
        app.use('/user', validator.middleware, require('./pages/user'));
    }
}

module.exports = exportValues;
