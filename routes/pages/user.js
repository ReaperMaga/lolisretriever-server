const express = require('express');
const router = express.Router();

const userManager = require("../../src/database/user/user_manager")


list = async function(req, res) {
    let list;
    await userManager.listUsers(function (docs) {
        list = docs;

    })
    if(list == null) {
        return res.status(404).json({err: "Not found"})
    }
    return res.status(200).json(list)
}

createUser = async function(req, res) {
    let name = req.params.name;
    let password = req.params.password;

    let docExists = false;
    await userManager.existsUser(name, function (exists) {
        docExists = exists;

    });
    if(!docExists) {
        userManager.createUser(name, password, function (doc, err) {
            if(err) {
                res.status(404).json({success: false})
                return;
            }
            res.status(200).json(doc);
        })
    } else {
        res.status(404).json({success: false})
    }

}

deleteUser = async function(req, res) {
    let name = req.params.name;
    await userManager.deleteUser(name, function (result, err) {
        if(err) {
            res.status(404).json({success: false, result: result})
            return;
        }
        res.status(200).json(result);
    })
}

findUser = async function(req, res) {
    let name = req.params.name;
    await userManager.findUser(name, function (doc) {
        if(doc != null) {
            res.status(200).json(doc);
        } else {
            res.status(404).json({success: false, result: "User not found"})
        }
    })
}


/** Init Routes **/
router.get('/list', list);
router.get('/find/:name', findUser)
router.post("/create/:name/:password", createUser)
router.post("/delete/:name", deleteUser)

module.exports = router;
