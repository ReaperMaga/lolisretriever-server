const token = "KJnp6v5EUcHmxjzra65qcuVdQ";

const middleware = function (req, res, next) {
    if(getHeader(req) === token) {
        next();
    }
};

function getHeader(req) {
    if(req.headers.lolisauth != null) {
        return req.headers.lolisauth;
    }
    return "None";
}

module.exports = {middleware}
