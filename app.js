let express = require('express');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
const routeManager = require("./routes/route_manager")



const main = require("./src/main")

const helmet = require("helmet")

let app = express();


/**
 * Init
 */
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(helmet())
//app.use(express.static(path.join(__dirname, 'public')));

/**
 * Main Source Project Start
 */
main.start()

/**
 * Routes
 */

routeManager.init(app)




module.exports = app;
