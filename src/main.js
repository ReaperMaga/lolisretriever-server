const mongoose = require("mongoose")
const databaseEvents = require("./database/events/database_events")
const databaseConnection = require("./database/database_connection")

const fileManager = require("./file/file_manager")


const credentials = new databaseConnection.Credentials("localhost", 27017, "lolisretriever");

const exportValues = {

    start() {
        databaseEvents.setConnectEvent(function (database) {
            console.log("Successfully connected to database on " + credentials.host + ":" + credentials.port)
        });

        databaseConnection.connect(credentials)

        //fileManager.writeFile("", "database.json", JSON.stringify(credentials, null, 4), function () {})

       /* fileManager.readFile("", "database.json", function (value) {
            console.log(value.databaseName)
        })*/
    }

}

module.exports = exportValues;
