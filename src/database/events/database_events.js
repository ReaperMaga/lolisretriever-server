
let connectEvent;
let disconnectEvent;

const exportValues = {
    connectEvent, disconnectEvent,

    setConnectEvent(event) {
        this.connectEvent = event;
    },
    setDisconnectEvent(event) {
        this.disconnectEvent = event;
    }
}

module.exports = exportValues;
