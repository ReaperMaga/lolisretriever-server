const mongoose = require("mongoose");
const databaseEvents = require("./events/database_events")

let database;

class Credentials {

    constructor(host, port, databaseName) {
        this.host = host;
        this.port = port;
        this.databaseName = databaseName;
    }

}

const exportValues = {
    mongoose,
    database,
    Credentials,
    connect(credential) {
        mongoose.connect("mongodb://"+credential.host+":"+credential.port+"/" + credential.databaseName, {useNewUrlParser: true, useUnifiedTopology: true})
        this.database = mongoose.connection;
        this.database.on("error", console.error.bind(console, "Verbindungsfehler: "));
        this.database.once("open", function () {
            databaseEvents.connectEvent(this.database);
        });
    }
}

module.exports = exportValues;
