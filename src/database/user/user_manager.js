const model = require("../model/user_model.js")

const crypto = require('crypto')


const exportValues = {

    hash(value) {
        let hash = crypto.createHash('md5').update(value).digest("hex")
        return hash;
    },

    createUser(name, password, finishCallback) {
        let document = new model.UserModel({name: name, password: password});
        document.save(function (err) {
            if (err) {
                document = null;
            }
            finishCallback(document, err);
        });
    },

    async existsUser(name, callback) {
        await model.UserModel.findOne({"name": name}, function (err, doc) {
            if (doc != null) {
                callback(true);
            } else {
                callback(false);
            }
        });
    },
    async listUsers(callback) {
        await model.UserModel.find({}, function (err, docs) {
            callback(docs);
        });
    },

    async deleteUser(name, callback) {
        await model.UserModel.deleteOne({name: name}, function (err, result) {
            callback(result, err)
        })
    },
    async findUser(name, callback) {
        await model.UserModel.findOne({"name": name}, function (err, doc) {
            callback(doc);
        });
    }
}

module.exports = exportValues;
