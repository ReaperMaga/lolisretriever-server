const databaseConnection = require("../database_connection.js")

const schema = new databaseConnection.mongoose.Schema({
    name: String,
    password: String
});


const UserModel = databaseConnection.mongoose.model("User", schema);

module.exports = {
    schema, UserModel
}
