let fileSystem = require("fs")


const exportValues = {
    writeFile(path, fileName, content, callback) {
        fileSystem.writeFile(path + fileName, content, "utf8", callback);
    },

    existsFile(path, fileName) {
      return fileSystem.existsSync(path + fileName);
    },

    readFile(path, fileName, file) {
        fileSystem.readFile(path + fileName, "utf8", function (err, data) {
            if(err) {
                console.error(err);
                return;
            }

            let value = JSON.parse(data);
            file(value);
        })
    }
}

module.exports = exportValues;
